﻿using System.Net;
using System.Net.Sockets;
using System.Text;

IPAddress ipAddress = new IPAddress(0xFFFFFFFF);

// 自分のIPアドレスを取得する
string hostname = Dns.GetHostName();
IPAddress[] selfIPAddress = Dns.GetHostAddresses(hostname);
foreach (IPAddress address in selfIPAddress)
{
    if (address.AddressFamily == AddressFamily.InterNetwork)
    {
        ipAddress = address;
    }
}

// エンドポイント（IPアドレスとポートの組み合わせ）を作成する
IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, 11_000);

// ソケットを作成する
using Socket listener = new(
    ipEndPoint.AddressFamily,
    SocketType.Stream,
    ProtocolType.Tcp);

// ソケットをバインドする
listener.Bind(ipEndPoint);
// 接続待ち状態にする
listener.Listen(100);
var handler = await listener.AcceptAsync();

// メッセージ送信タスクを生成
Action ASendMsg = startSend;
Task TSendTask = new Task(ASendMsg);
TSendTask.Start();
Console.WriteLine("メッセージを入力してください。");

while (true)
{
    // 同期でメッセージを受信する
    var buffer = new byte[1_024];
    var received = await handler.ReceiveAsync(buffer, SocketFlags.None);
    var response = Encoding.UTF8.GetString(buffer, 0, received);

    // クライアントのACKメッセージを受信
    if (response == "<|ACK|>")
    {
        Console.WriteLine("送信成功！");
    }
    // クライアントの通常メッセージを受信
    else
    {
        Console.WriteLine($"Receive message:\"{response}\"");

        // メッセージでACKを送信
        var ackMessage = "<|ACK|>";
        var echoBytes = Encoding.UTF8.GetBytes(ackMessage);
        await handler.SendAsync(echoBytes, 0);
    }
}

void startSend()
{
    while (true)
    {
        // 同期でメッセージを送信する
        var message = Console.ReadLine();
        if (message != null)
        {
            var messageBytes = Encoding.UTF8.GetBytes(message);
            _ = handler.SendAsync(messageBytes, SocketFlags.None);
        }
    }
}