﻿using System.Net.Sockets;
using System.Net;
using System.Text;

IPAddress ipAddress = new IPAddress(0xFFFFFFFF);

// 正しいIPアドレスを入力させる
bool ret = false;
while (!ret)
{
    Console.WriteLine("Input server IP address");
    var inIP = Console.ReadLine();
    ret = IPAddress.TryParse(inIP, out ipAddress);

    if (!ret) Console.WriteLine($"Wrong IP address({inIP}).Input again.");
}

// エンドポイント（IPアドレスとポートの組み合わせ）を作成する
IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, 11_000);

// ソケットを作成する
using Socket client = new Socket(
    ipEndPoint.AddressFamily,
    SocketType.Stream,
    ProtocolType.Tcp);

// 同期で接続を試みる
await client.ConnectAsync(ipEndPoint);

// メッセージ送信タスクを生成
Action ASndMsg = startSend;
Task TSndTask = new Task(ASndMsg);
TSndTask.Start();
Console.WriteLine("メッセージを入力してください。");

while (true)
{
    // 同期でメッセージを受信する
    var buffer = new byte[1_024];
    var received = await client.ReceiveAsync(buffer, SocketFlags.None);
    var response = Encoding.UTF8.GetString(buffer, 0, received);

    if (response == "<|ACK|>")
    {
        Console.WriteLine("送信成功！");
    }
    else
    {
        Console.WriteLine($"Receive message:\"{response}\"");

        // メッセージでACKを送信
        var ackMessage = "<|ACK|>";
        var echoBytes = Encoding.UTF8.GetBytes(ackMessage);
        await client.SendAsync(echoBytes, 0);
    }
}

void startSend()
{
    // 同期でメッセージを送信する
    while (true)
    {
        var message = Console.ReadLine();
        if (message != null)
        {
            var messageBytes = Encoding.UTF8.GetBytes(message);
            _ = client.SendAsync(messageBytes, SocketFlags.None);
        }
    }
}